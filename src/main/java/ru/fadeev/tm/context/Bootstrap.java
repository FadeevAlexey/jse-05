package ru.fadeev.tm.context;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.command.applicationcommand.ExitCommand;
import ru.fadeev.tm.command.applicationcommand.HelpCommand;
import ru.fadeev.tm.command.projectcommand.*;
import ru.fadeev.tm.command.taskcommand.*;
import ru.fadeev.tm.exception.EntityDuplicateException;
import ru.fadeev.tm.repository.ProjectRepository;
import ru.fadeev.tm.repository.TaskRepository;
import ru.fadeev.tm.service.ProjectService;
import ru.fadeev.tm.service.TaskService;
import ru.fadeev.tm.util.Helper;

import java.util.*;

public final class Bootstrap {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final TaskRepository taskRepository = new TaskRepository();

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    public void init() {
        registry(new ProjectCreateCommand(this));
        registry(new ProjectListCommand(this));
        registry(new ProjectRemoveCommand(this));
        registry(new ProjectClearCommand(this));
        registry(new ProjectEditCommand(this));
        registry(new ProjectTasksCommand(this));
        registry(new TaskCreateCommand(this));
        registry(new TaskListCommand(this));
        registry(new TaskRemoveCommand(this));
        registry(new TaskClearCommand(this));
        registry(new TaskEditCommand(this));
        registry(new TaskAddProjectCommand(this));
        registry(new ExitCommand(this));
        registry(new HelpCommand(this));
        start();
    }

    private void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            try {
                command = Helper.readString();
                execute(command);
            } catch (EntityDuplicateException e) {
                System.out.println("this id already exists, try again");
            }
        }
    }

    private void registry(final AbstractCommand command) {
        final String cliCommand = command.getName();
        final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty()) {
            System.out.println("Wrong command name");
            return;
        }
        if (cliDescription == null || cliDescription.isEmpty()) return;
        commands.put(cliCommand, command);
    }

    private void execute(final String command) {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            System.out.println("Wrong command name");
            return;
        }
        abstractCommand.execute();
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

}