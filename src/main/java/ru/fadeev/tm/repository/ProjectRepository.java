package ru.fadeev.tm.repository;

import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.exception.EntityDuplicateException;

import java.util.*;

public class ProjectRepository {

    private Map<String, Project> projects = new LinkedHashMap<>();

    public List<Project> findAll() {
        return new LinkedList<>(projects.values());
    }

    public Project findOne(String id) {
        return projects.get(id);
    }

    public Project remove(String id) {
      return projects.remove(id);
    }

    public Project persist(Project project) {
        if (projects.containsKey(project.getId()))
            throw new EntityDuplicateException();
        return projects.put(project.getId(), project);
    }

    public Project merge(Project project) {
        return projects.put(project.getId(), project);
    }

    public void removeAll() {
        projects.clear();
    }

}