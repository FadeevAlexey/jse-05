package ru.fadeev.tm.command.projectcommand;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.service.ProjectService;
import ru.fadeev.tm.service.TaskService;
import ru.fadeev.tm.util.Helper;

public final class ProjectRemoveCommand extends AbstractCommand {

    public ProjectRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        ProjectService projectService = bootstrap.getProjectService();
        System.out.println("[PROJECT REMOVE]\nENTER NAME:");
        String projectId = projectService.findIdByName(Helper.readString());
        if (projectId == null) {
            System.out.println("Can't find project");
            return;
        }
        projectService.remove(projectId);
        removeAllTasks(projectId);
        System.out.println("[OK]\n");
    }

    private void removeAllTasks(String projectId) {
        TaskService taskService = bootstrap.getTaskService();
        taskService.findAll()
                .stream()
                .filter(task -> task.getProjectId().equals(projectId))
                .forEach(task -> taskService.remove(task.getId()));
    }

}