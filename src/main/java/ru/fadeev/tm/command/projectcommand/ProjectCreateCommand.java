package ru.fadeev.tm.command.projectcommand;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.util.Helper;

public final class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        String name = Helper.readString();
        Project project = new Project();
        project.setName(name);
        bootstrap.getProjectService().persist(project);
        System.out.println("[OK]\n");
        System.out.println("WOULD YOU LIKE EDIT PROPERTIES PROJECT ? USE COMMAND project-edit\n");
    }

}