package ru.fadeev.tm.command.projectcommand;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.service.ProjectService;
import ru.fadeev.tm.util.Helper;

import java.util.Date;

public final class ProjectEditCommand extends AbstractCommand {

    public ProjectEditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-edit";
    }

    @Override
    public String getDescription() {
        return "Edit project.";
    }

    @Override
    public void execute() {
        ProjectService projectService = bootstrap.getProjectService();
        System.out.println("[EDIT PROJECT]");
        System.out.println("ENTER CURRENT NAME:");
        String name = Helper.readString();
        Project project = projectService.findOne(projectService.findIdByName(name));
        if (project == null){
            System.out.println("can't find project with name "+ name);
            return;
        }
        fillFields(project);
        projectService.merge(project);
        System.out.println("[OK]\n");
    }

    private void fillFields(Project project) {
        System.out.println("YOU CAN ADD EDIT DESCRIPTION OR PRESS ENTER");
        String description = Helper.readString();
        if (description != null && !description.isEmpty()) project.setDescription(description);
        System.out.println("YOU CAN ADD EDIT START DATE OR PRESS ENTER");
        Date startDate = Helper.readDate();
        if (startDate != null) project.setStartDate(startDate);
        System.out.println("YOU CAN ADD EDIT FINISH DATE OR PRESS ENTER");
        Date finishDate = Helper.readDate();
        if (finishDate != null) project.setFinishDate(finishDate);
    }

}