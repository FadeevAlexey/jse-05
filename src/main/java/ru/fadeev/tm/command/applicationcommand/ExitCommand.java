package ru.fadeev.tm.command.applicationcommand;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;

public final class ExitCommand extends AbstractCommand {

    public ExitCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit from the program.";
    }

    @Override
    public void execute() {
        System.out.println("Thank you for using Task Manager. See you soon.");
    }

}