package ru.fadeev.tm.command;

import ru.fadeev.tm.context.Bootstrap;

public abstract class AbstractCommand {

    protected Bootstrap bootstrap;

    public AbstractCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract void execute();

    public abstract String getName();

    public abstract String getDescription();

}