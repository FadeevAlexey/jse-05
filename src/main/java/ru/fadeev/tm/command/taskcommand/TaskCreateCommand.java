package ru.fadeev.tm.command.taskcommand;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.util.Helper;

public final class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        String name = Helper.readString();
        Task task = new Task();
        task.setName(name);
        bootstrap.getTaskService().persist(task);
        System.out.println("[OK]\n");
        System.out.println("WOULD YOU LIKE EDIT PROPERTIES TASK? USE COMMAND task-edit\n");
    }

}