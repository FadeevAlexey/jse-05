package ru.fadeev.tm.command.taskcommand;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.service.TaskService;
import ru.fadeev.tm.util.Helper;

import java.util.Date;

public final class TaskEditCommand extends AbstractCommand {

    public TaskEditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-edit";
    }

    @Override
    public String getDescription() {
        return "Edit project.";
    }

    @Override
    public void execute() {
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER CURRENT NAME:");
        String name = Helper.readString();
        Task task = taskService.findOne(taskService.findIdByName(name));
        if (task == null) {
            System.out.println("can't find project with name " + name);
            return;
        }
        fillFields(task);
        taskService.merge(task);
        System.out.println("[OK]\n");
        System.out.println("WOULD YOU LIKE ADD PROJECT TO TASK ? USE COMMAND task-addProject\n");
    }

    private void fillFields(Task task) {
        System.out.println("YOU CAN ADD EDIT DESCRIPTION OR PRESS ENTER");
        String description = Helper.readString();
        System.out.println("YOU CAN ADD EDIT START DATE OR PRESS ENTER");
        Date startDate = Helper.readDate();
        System.out.println("YOU CAN ADD EDIT FINISH DATE OR PRESS ENTER");
        Date finishDate = Helper.readDate();
        if (description != null && !description.isEmpty()) task.setDescription(description);
        if (startDate != null) task.setStartDate(startDate);
        if (finishDate != null) task.setFinishDate(finishDate);
    }

}