package ru.fadeev.tm.command.taskcommand;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.service.ProjectService;
import ru.fadeev.tm.service.TaskService;
import ru.fadeev.tm.util.Helper;

public final class TaskAddProjectCommand extends AbstractCommand {

    public TaskAddProjectCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-addProject";
    }

    @Override
    public String getDescription() {
        return "Adding task to the project.";
    }

    @Override
    public void execute() {
        ProjectService projectService = bootstrap.getProjectService();
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("[ADD TASK TO PROJECT]");
        System.out.println("ENTER TASK NAME");
        String taskId = taskService.findIdByName(Helper.readString());
        System.out.println("ENTER PROJECT NAME");
        String projectId = projectService.findIdByName(Helper.readString());
        if (taskId == null || projectId == null) {
            System.out.println("Can't find task or project");
            return;
        }
        Task task = taskService.findOne(taskId);
        task.setProjectId(projectId);
        taskService.merge(task);
        System.out.println("[OK]\n");
    }

}